<?php

return [
    /*
    |--------------------------------------------------------------------------
    | OPEN WEATHER CONFIGURATION
    |--------------------------------------------------------------------------
    */

     
        'key' => env('OPEN_WEATHER_KEY', 'f43e241eac1bab1aaf4724fc8393be72'),
        'endpoint' => env('OPEN_WEATHER_ENDPOINT', 'weather'),
        'uri' => env('OPEN_WEATHER_URI', 'http://api.openweathermap.org/data/2.5/')
    
    ];