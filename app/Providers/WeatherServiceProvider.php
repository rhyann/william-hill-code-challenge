<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class WeatherServiceProvider extends ServiceProvider
{
    /**
     * Register weather services
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Weather\WeatherInterface', 'App\Weather\WeatherInterfaceImpl');
    }
}