<?php

namespace App\Weather;

interface WeatherInterface
{
    /*
    |--------------------------------------------------------------------------
    | Weather Interface
    |--------------------------------------------------------------------------
    |
    | This interface is defines methods for weather requests. 
    |
    */

    /**
     * Gets wind information by zip code.
     *
     * @param string $zipCode
     * @return Wind 
     */
    public function wind(string $zipCode);
}