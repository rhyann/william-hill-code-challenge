<?php

namespace App\Weather;

use App\Http\Adapters\WeatherClient as Weather;
use Config;
use App\Model\Wind;

class WeatherInterfaceImpl implements WeatherInterface 
{
    
    /**
     * Wind Implementation
     * @param string $zipCode
     * @return Wind 
     */
    public function wind(string $zipCode)
    {
        $weather = new Weather();
        $response = $weather->request('GET', config('weather.endpoint'), $zipCode)['wind'];
        $wind = new Wind($response);
        return $wind;
    }
}