<?php

namespace App\Http\Adapters;

use GuzzleHttp\Client;
use Config;
use Illuminate\Support\Facades\Cache;

class WeatherClient
{
    /**
     * Guzzle client
     * 
     */
    protected $client;

    /**
     * Instantiates a new weather client
     * 
     */
    public function __construct()
    {
        $this->client = new Client(["base_uri" => config('weather.uri')]);
    }

    /**
     * Performs an http request to weather endpoint(openweather.org)
     * 
     * @param string $method
     * @param string $uri
     * @param string $query
     * @return json
     */
    public function request(string $method, $uri, string $query)
    {
        $json = Cache::get($query);
        if($json == null)
        {
            $params = [
                'query' => [
                    'q' => $query,
                    'APPID' => config('weather.key')
                ],
                'debug' => false
            ];
            $response = $this->client->request($method, $uri, $params);
            $json = json_decode($response->getBody(), true);
            Cache::store('file')->put($query, $json, 15);
        }
        return $json;
    }


}