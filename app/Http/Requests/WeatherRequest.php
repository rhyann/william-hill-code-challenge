<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WeatherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation Rules that apply to request
     * 
     * @return array
     */
    public function rules()
    {
        return [
            'zipCode' => 'required|digits:5',
        ];
    }

    /**
     * Custom message for validation
     * 
     * @return array
     */
    public function messages()
    {
        return [
            'zipCode.required' => 'Zip code is required',
            'zipCode.digit' => 'Zip code must be a number'
        ];
    }

    /**
     * Use route parameters for validation
     * @return array
     */
    protected function validationData()
    {
        return $this->route()->parameters();
    }
}
