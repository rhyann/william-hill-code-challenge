<?php

namespace App\Http\Controllers\Weather;

use App\Http\Controllers\Controller;
use App\Weather\WeatherInterface as Weather;
use JMS\Serializer\SerializerBuilder;
use App\Http\Requests\WeatherRequest;

class WeatherController extends Controller 
{
    /*
    |--------------------------------------------------------------------------
    | Weather Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling weather requests. 
    |
    */

    /**
     * 
     * 
     */
    protected $weather;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Weather $weather)
    {
        $this->weather = $weather;
    }

    /**
     * 
     *
     * @param string $zipCode
     * @return response
     */
    public function wind(WeatherRequest $request, $zipCode)
    {
       
        $validated = $request->validated();
        $serializer = SerializerBuilder::create()->build();
        return response($serializer->serialize($this->weather->wind($zipCode), 'json'))
                        ->header('Content-Type', 'application/json');
    }
}