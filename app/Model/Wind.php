<?php

namespace App\Model;



/**
 * Wind Model
 */
class Wind
{
    /**
     * Wind Speed
     * 
     */
    private $speed;

    /**
     * Wind Direction
     * 
     */
    private $direction;

    /**
     * Creates an instance of wind
     * @param array $wind
     */
    public function __construct(array $wind)
    {
        $this->speed = $wind['speed'];
        $this->direction = $wind['deg'];
    }

}