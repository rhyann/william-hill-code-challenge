<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\Weather\WeatherController;
use Illuminate\Support\Facades\Cache;
use App\Http\Adapters\WeatherClient;
use Config;
use App\Http\Requests\WeatherRequest;
use Mockery;
use Illuminate\Routing\Route;

class WeatherTest extends TestCase
{
   
    /**
     * Test WeatherController@wind.
     *
     * @return void
     */
    public function testWind()
    {
        $requestMock = Mockery::mock(WeatherRequest::class)
            ->makePartial()
            ->shouldReceive('path')
            ->once()
            ->andReturn('api/v1/wind/89149');
        app()->instance('request', $requestMock->getMock());
        $request = request();
        $request->setRouteResolver(function () use ($request) {
            return (new Route('GET', 'api/v1/wind/{zipCode}', []))->bind($request);
        });
        $this->assertTrue($request->route()->parameter('zipCode') == '89149');
    }

    /**
     * Test Cache.
     * 
     * @return void
     */
    public function testWindCache()
    {
        Cache::shouldReceive('get')
                    ->once()
                    ->with('89149')
                    ->andReturn(true);       
        $client = new WeatherClient();
        $json = $client->request('GET', config('weather.endpoint'), '89149');  
    }
}
